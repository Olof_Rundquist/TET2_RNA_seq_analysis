#!/bin/bash
#SBATCH -J FeatureCounts
#SBATCH -N 1 # Nodes
#SBATCH -n 32 # Processes
#SBATCH -t 24:00:00

files=$1 # Input the files as a space delimited string
annotation=ERV.saf
output=HERV_counts.tsv

featureCounts -p -T 32 -F 'SAF' -a ${annotation} -o ${output} ${files}
