#!/bin/bash -l

#SBATCH -J Stringtie
#SBATCH -n 32
#SBATCH -N 1
#SBATCH -t 01:00:00

bam=$1

stringtie -e -p 32 -G /proj/lassim/reference_genomes_OR/human/hg38_refseq/GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gtf -o ${bam%%.bam}/expression.gtf $bam
