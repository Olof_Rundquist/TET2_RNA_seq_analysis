#!/bin/bash -l

#SBATCH -J Stringtie
#SBATCH -n 32
#SBATCH -N 1
#SBATCH -t 00:10:00

bam=$1
gtf=/proj/lassim/reference_genomes_OR/human/hg38_refseq/GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gtf
outdir=$(echo $bam | cut -d . -f1,2)

stringtie -eB -p 32 -G $gtf -o ${outdir}/expression.gtf $bam
