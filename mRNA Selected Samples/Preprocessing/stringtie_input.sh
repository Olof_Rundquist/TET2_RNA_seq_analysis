#!/bin/bash -l

for i in ./aligned/*bam
do
	sbatch stringtie.sh ${i}
done
