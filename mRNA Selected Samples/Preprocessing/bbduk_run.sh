#!/bin/bash -l

#SBATCH -n 32
#SBATCH -t 00:05:00

bbduk.sh threads=32 in=$2 out=$1 ref=adapters ktrim=r trimq=20
