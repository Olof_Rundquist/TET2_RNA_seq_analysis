#!/bin/bash -l

#SBATCH -J STAR
#SBATCH -n 32
#SBATCH -t 00:15:00

samples=${1}
outname=${2}
genomedir=/proj/lassim/reference_genomes_OR/human/hg38_refseq/star_index

STAR --runThreadN 32 --outSAMtype BAM SortedByCoordinate --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --readFilesCommand zcat --genomeDir $genomedir --readFilesIn ${samples} --outTmpDir ${outname}.tmp. --outFileNamePrefix ${outname}

sbatch stringtie_run.sh ${outname}*bam
