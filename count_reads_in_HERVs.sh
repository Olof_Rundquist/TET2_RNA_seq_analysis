#!/bin/bash
#SBATCH -J FeatureCounts
#SBATCH -N 1 # Nodes
#SBATCH -n 32 # Processes
#SBATCH -t 24:00:00

# Convert the erv.bed file from HERVd to SAF format
awk '{print $4"\t"$1"\t"$2"\t"$3"\t"$6}' erv.bed > ERV.saf

files=$1 # Input the files as a space delimited string (Use find)
annotation=ERV.saf
output=HERV_counts.tsv

featureCounts -p --primary -T 32 -F 'SAF' -a ${annotation} -o ${output} ${files}
