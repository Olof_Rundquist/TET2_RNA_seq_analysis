## Total RNA-seq analysis
Code author and maintainer: Olof Rundquist

### Summary
The following repository contains all code that was used to make the figures for the Total RNA-seq presented in the PNAS article (DOI: 10.1073/pnas.2110758118). Any figure or table not present in the code was created indepedently by Maike Bensberg using the information provided in the Test tabels produced by the "Proccess_data_Sleuth.R" and "HERV_Limma_analysis.R" scripts.

#### Gene quantification with Salmon and Sleuth
The expresstion of the genes were quantified using Salmon v0.14.1. See the provided scripts, Salmon_input.sh and Salmon_run.sh. 
The Salmon output was then converted to kalisto format using "wasabi" for processing with Sleuth in R. See prepare_fish.sh.
Diffrential analysis was then performed using Sleuth as outlined in Proccess_data_Sleuth.R and figures constructed with the Making_of_figures_for_Sleuth.R script. 

#### HERV expression quantification
For the HERV expresstion reads were first aligend to the Hg38 version of the human genome using the aligner STAR. See the provided scripts, STAR_input.sh and star_PE.sh. HERV expresstion was then quantified with featurecounts from the Rsubread package using the annotation form the HERVd database (https://herv.img.cas.cz/). See count_reads_in_HERVs.sh. 
Limma analysis was then carried out in a similar fashion to the analysis perfomred on the genes with Sleuth to defeine diffrentially expressed HERVs. See HERV_Limma_analysis.R.

##### Figure S1C
The figure S1C was constructed thusly: Reads from the Samples (See supplementary data 2 in the papper) were aligned with STAR as with the parameters presented in star_PE.sh. Read coverage and gene models were then ploted using Gviz, see the Fetal_Coverage_Tracks_Gviz.R script. The splice junctions were created by loading the aligned files into IGV and setting the diplay paramter for splice junctions at at least 50 reads. This cutoff was set to avoid the display of low coverage splice junction to theoretical exons present in the underlying genome assembly. There was not a single read in any sample covering the TET2b specific splice junction between TET2b exon 1 and 2. Conservation for the TET2 gene region was aquired from the UCSC genome browser.
