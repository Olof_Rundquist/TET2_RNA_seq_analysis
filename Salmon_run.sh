#!/bin/bash -l

#SBATCH -J Salmon-quant
#SBATCH -n 32
#SBATCH -N 1
#SBATCH -t 01:00:00

index=${4}

forward=${1}
reverse=${2}
out=${3}

salmon quant -p 32 -i $index -l IU -1 ${forward} -2 ${reverse} --output $out --numBootstraps 100 --validateMappings --seqBias --gcBias
