#!/bin/bash -l

mkdir -p STAR
Samples=$(find ./fastq/* -type d -prune)

for i in $Samples
do 
        Sample_name=$(echo ${i} | cut -d / -f3)
        forward=$(find ${i}/*_1.fq.gz)
        reverse=$(find ${i}/*_2.fq.gz)
        # Run for normal
        sbatch star_PE.sh $forward $reverse ./STAR/${Sample_name}
done
