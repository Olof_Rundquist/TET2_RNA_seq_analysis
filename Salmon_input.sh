#!/bin/bash -l

Samples=$(find ./fastq/* -type d -prune)
index=/proj/lassim/reference_genomes_OR/human/hg38_refseq/salmon_NM_NR_only_index
SalmonTE_index=/proj/lassim/reference_genomes_OR/human/hg38_refseq/Salmon_HERV_merged_index/NM_NR_only_index

for i in $Samples
do 
        Sample_name=$(echo ${i} | cut -d / -f3)
        forward=$(find ${i}/*_1.fq.gz)
        reverse=$(find ${i}/*_2.fq.gz)
        # Run for normal
        mkdir -p ./Salmon/${Sample_name}
        sbatch Salmon_run.sh $forward $reverse ./Salmon/${Sample_name} $index
done
